import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DebugDemoWithUI {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame("Debug Demo with UI");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(300, 200);

            JPanel panel = new JPanel();
            frame.add(panel);

            JButton addButton = new JButton("Сложение");
            JButton multiplyButton = new JButton("Умножение");
            JLabel resultLabel = new JLabel("Результат: ");

            panel.add(addButton);
            panel.add(multiplyButton);
            panel.add(resultLabel);

            addButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int a = 5;
                    int b = 7;
                    int sum = add(a, b);
                    resultLabel.setText("Результат сложения: " + sum);
                }
            });

            multiplyButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int a = 5;
                    int b = 7;
                    int product = multiply(a, b);
                    resultLabel.setText("Результат умножения: " + product);
                }
            });

            frame.setVisible(true);
        });
    }

    public static int add(int x, int y) {
        int result = x + y;
        return result;
    }

    public static int multiply(int x, int y) {
        int result = x * y;
        return result;
    }
}